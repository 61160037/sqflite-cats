import 'cat.dart';
import 'cat_dao.dart';

void main() async {
  var ray = Cat(
    id: 0,
    name: 'ray',
    age: 1,
  );
  var uta = Cat(
    id: 1,
    name: 'uta',
    age: 5,
  );

  await CatDao.insertCat(ray);
  await CatDao.insertCat(uta);

  print(await CatDao.cats());

  ray = Cat(
    id: ray.id,
    name: ray.name,
    age: ray.age + 2,
  );
  await CatDao.updateCat(ray);

  print(await CatDao.cats());

  await CatDao.deleteCat(0);

  print(await CatDao.cats());
}
